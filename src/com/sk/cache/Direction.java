package com.sk.cache;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 01/03/14
 * Time: 14:49
 */
public enum Direction {
	NORTH, EAST, SOUTH, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST
}
