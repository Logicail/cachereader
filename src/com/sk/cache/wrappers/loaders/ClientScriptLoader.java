package com.sk.cache.wrappers.loaders;

import com.sk.cache.fs.Archive;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.fs.CacheType;
import com.sk.cache.fs.FileData;
import com.sk.cache.wrappers.ClientScript;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 23/02/14
 * Time: 19:19
 */
public class ClientScriptLoader extends WrapperLoader<ClientScript> {

	private CacheType cache;

	public ClientScriptLoader(CacheSystem cacheSystem) {
		super(cacheSystem);
		cache = cacheSystem.getCacheSource().getCacheType(17);
	}

	@Override
	public ClientScript load(int id) {
		Archive archive = cache.getArchive(id >>> 8);
		if (archive == null)
			throw new IllegalArgumentException("Bad script id " + id);
		final FileData file = archive.getFile(id & 0xff);
		ClientScript script = new ClientScript(this, id);
		script.decode(file.getDataAsStream());
		return script;
	}

	@Override
	public boolean canLoad(int id) {
		return cache.getArchive(id >>> 8).getFile(id & 0xff) != null;
	}
}
