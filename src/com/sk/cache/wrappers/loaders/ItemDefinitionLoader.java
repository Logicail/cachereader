package com.sk.cache.wrappers.loaders;

import com.sk.cache.fs.CacheSystem;
import com.sk.cache.fs.FileData;
import com.sk.cache.wrappers.ItemDefinition;

public class ItemDefinitionLoader extends ProtocolWrapperLoader<ItemDefinition> {

	private static final String[] antiEdible = {"Burnt", "Rotten", "Poison", "Fish-like thing",
			"Dwarven rock cake"};

	public ItemDefinitionLoader(CacheSystem cacheSystem) {
		super(cacheSystem, cacheSystem.getCacheSource().getCacheType(19));
	}

	public ItemDefinitionLoader(CacheSystem cacheSystem, int cacheType) {
		super(cacheSystem, cacheSystem.getCacheSource().getCacheType(cacheType));
	}

	@Override
	public ItemDefinition load(int id) {
		FileData data = getValidFile(id);
		ItemDefinition ret = new ItemDefinition(this, id);
		ret.decode(data.getDataAsStream());
		fixItem(ret);
		return ret;
	}


	private void fixItem(ItemDefinition item) {
		if (item.noteTemplateId != null) {
			item.fixNoted(load(item.noteTemplateId), load(item.noteId));
		}
		if (item.lentTemplateId != null && item.lentId != null) {
			item.fixLent(load(item.lentTemplateId), load(item.lentId));
		}
		if (item.cosmeticTemplateId != null && item.cosmeticTemplateId != -1) {
			item.fixCosmetic(load(item.cosmeticTemplateId), load(item.cosmeticId));
		}
	}


//	private boolean isEdible(ItemDefinition item) {
//		if (Arrays.asList(item.actions).contains("Eat") && item.name != null) {
//			for (String s : antiEdible) {
//				if (item.name.contains(s)) {
//					return false;
//				}
//			}
//			return true;
//		}
//		return false;
//	}

}
