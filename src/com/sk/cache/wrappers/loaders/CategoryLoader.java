package com.sk.cache.wrappers.loaders;

import com.sk.cache.fs.Archive;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.fs.FileData;
import com.sk.cache.wrappers.CategoryDefinition;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 16/04/2014
 * Time: 19:41
 */
public class CategoryLoader extends WrapperLoader<CategoryDefinition> {
	private final Archive source;

	public CategoryLoader(CacheSystem cacheSystem) {
		super(cacheSystem);
		source = cacheSystem.getCacheSource().getCacheType(10).getArchive(1);
	}

	@Override
	public CategoryDefinition load(int id) {
		FileData data = source.getFile(id);
		if (data == null)
			throw new IllegalArgumentException("Bad quest id");
		CategoryDefinition ret = new CategoryDefinition(this, id);
		ret.decode(data.getDataAsStream());
		return ret;
	}

	@Override
	public boolean canLoad(int id) {
		return source.getFile(id) != null;
	}
}
