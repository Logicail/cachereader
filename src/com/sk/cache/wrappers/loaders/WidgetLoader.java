package com.sk.cache.wrappers.loaders;

import com.sk.cache.fs.Archive;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.fs.CacheType;
import com.sk.cache.fs.FileData;
import com.sk.cache.wrappers.Widget;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 27/02/14
 * Time: 15:02
 */
public class WidgetLoader extends WrapperLoader<Widget> {
	private CacheType cache;

	public WidgetLoader(CacheSystem cacheSystem) {
		super(cacheSystem);
		cache = cacheSystem.getCacheSource().getCacheType(3);
	}

	@Override
	public Widget load(int id) {
		Archive archive = cache.getArchive(id >>> 8);
		if (archive == null)
			throw new IllegalArgumentException("Bad widget id " + id);
		final FileData file = archive.getFile(id & 0xff);
		Widget script = new Widget(this, id);
		script.decode(file.getDataAsStream());
		return script;
	}

	@Override
	public boolean canLoad(int id) {
		return false;
	}
}
