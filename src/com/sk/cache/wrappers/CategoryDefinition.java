package com.sk.cache.wrappers;

import com.sk.cache.wrappers.loaders.CategoryLoader;
import com.sk.cache.wrappers.protocol.ProtocolGroup;
import com.sk.datastream.Stream;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 16/04/2014
 * Time: 19:42
 */
public class CategoryDefinition extends ProtocolWrapper {
	private static final ProtocolGroup protocol = new ProtocolGroup();

	public CategoryDefinition(CategoryLoader loader, int id) {
		super(loader, id, protocol);
	}

	@Override
	public void decode(Stream stream) {
		int opcode;
		while ((opcode = stream.getUByte()) != 0) {
			System.out.println(Arrays.toString(stream.getAllBytes()));
		}
	}
}
