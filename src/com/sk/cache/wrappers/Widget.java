package com.sk.cache.wrappers;

import com.sk.cache.fs.Archive;
import com.sk.cache.wrappers.loaders.WidgetLoader;
import com.sk.datastream.Stream;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 27/02/14
 * Time: 15:02
 */
public class Widget extends StreamedWrapper {
	public boolean locked = false;
	public CacheImage disabledSprite;
	public int animDelay;
	public CacheImage sprites[];
	public static HashMap<Integer, Widget> cache;
	public LinkedList<Integer> children;
	public LinkedList<Integer> childX;
	public LinkedList<Integer> childY;
	//public static RSInterface cache[];
	public int requiredValues[];
	public int contentType;
	public int spritesX[];
	public int disabledHoverColor;
	public int actionType;
	public String spellName;
	public int enabledColor;
	public int width;
	public String tooltip;
	public String selectedActionName;
	public boolean centered;
	public int scrollPosition;
	public String actions[];
	public int valueIndexArray[][];
	public boolean filled;
	public String enabledText;
	public int hoverId;
	public int invSpritePadX;
	public int disabledColor;
	public int disabledMediaType;
	public int disabledMediaId;
	public boolean deletesTargetSlot;
	public int parentId;
	public int spellUsableOn;
	//protected static MemCache spriteNodes;
	public int enabledHoverColor;
	public boolean usableItemInterface;
	//public RSFont font;
	public int invSpritePadY;
	public int valueCompareType[];
	public int currentFrame;
	public int spritesY[];
	public String disabledText;
	public boolean isInventoryInterface;
	public int id;
	public int inventoryAmount[];
	public int inventory[];
	public byte alpha;
	public int enabledMediaType;
	public int enabledMediaId;
	public int disabledAnimation;
	public int enabledAnimation;
	public boolean itemsSwappable;
	//public RSImage enabledSprite;
	public int scrollMax;
	public int type;
	public int drawOffsetX;
	public int drawOffsetY;
	public boolean showInterface;
	public int height;
	public boolean shadowed;
	public int zoom;
	public int rotationX;
	public int rotationY;
	public String disabledSpriteArchive;
	public int disabledSpriteId;
	public String enabledSpriteArchive;
	public int enabledSpriteId;
	public String[] spriteNames;
	public int[] spriteIds;
	public int fontId;
	//public RSFont[] fonts;

//	public static CacheImage getSprite(String name, int id, Archive archive) {
//		Main.getInstance().mediaArchive.addKnownArchive(name);
//		long hash = (TextUtils.stringToLong(name) << 8) + (long) id;
//		RSImage sprite = null;
//		if (spriteNodes != null) {
//			sprite = (RSImage) spriteNodes.get(hash);
//			if (sprite != null) {
//				return sprite;
//			}
//		}
//		try {
//			sprite = new RSImage(archive, name, id);
//			if (spriteNodes != null) {
//				spriteNodes.put(sprite, hash);
//			}
//		} catch (Exception e) {
//			//e.printStackTrace();
//			return null;
//		}
//		return sprite;
//	}


	public void setDisabledSprite(String name, int id, Archive archive) {
		disabledSpriteArchive = name;
		disabledSpriteId = id;
		//disabledSprite = getSprite(name, id, archive);
	}

	public void setEnabledSprite(String name, int id, Archive archive) {
		enabledSpriteArchive = name;
		enabledSpriteId = id;
		//enabledSprite = getSprite(name, id, archive);
	}

	public void setSprites(int index, String name, int id, Archive archive) {
		spriteNames[index] = name;
		spriteIds[index] = id;
		//sprites[index] = getSprite(name, id, archive);
	}

	public boolean hasChildren() {
		return children != null;
	}

	public static Widget getInterface(int id) {
		return cache.get(id);
	}


	public Widget(WidgetLoader loader, int id) {
		super(loader, id);
	}

	@Override
	public void decode(Stream stream) {
		readHeader(stream);
		readType(stream);
	}

	private void readType(Stream buffer) {
		Object media = null;
		//RSFont[] fonts = {Main.getInstance().small, Main.getInstance().regular, Main.getInstance().bold, Main.getInstance().fancy};
		if (this.type == 0) {
			this.scrollMax = buffer.getUShort();
			this.showInterface = buffer.getUByte() == 1;
			int totalChildren = buffer.getUShort();
			this.children = new LinkedList<>();
			this.childX = new LinkedList<>();
			this.childY = new LinkedList<>();
			for (int index = 0; index < totalChildren; index++) {
				this.children.add(buffer.getUShort());
				this.childX.add((int) buffer.getShort());
				this.childY.add((int) buffer.getShort());
			}
		}
		if (this.type == 1) {
			buffer.getUShort();
			buffer.getUByte();
		}
		if (this.type == 2) {
			this.inventory = new int[this.width * this.height];
			this.inventoryAmount = new int[this.width * this.height];
			this.itemsSwappable = buffer.getUByte() == 1;
			this.isInventoryInterface = buffer.getUByte() == 1;
			this.usableItemInterface = buffer.getUByte() == 1;
			this.deletesTargetSlot = buffer.getUByte() == 1;
			this.invSpritePadX = buffer.getUByte();
			this.invSpritePadY = buffer.getUByte();
			this.spritesX = new int[20];
			this.spritesY = new int[20];
			this.sprites = new CacheImage[20];
			this.spriteNames = new String[20];
			this.spriteIds = new int[20];
			for (int index = 0; index < 20; index++) {
				int dummy = buffer.getUByte();
				if (dummy == 1) {
					this.spritesX[index] = buffer.getShort();
					this.spritesY[index] = buffer.getShort();
					String spriteInfo = buffer.getString();
					if (media != null && spriteInfo.length() > 0) {
						int comma = spriteInfo.lastIndexOf(",");
						int id = Integer.parseInt(spriteInfo.substring(comma + 1));
						String name = spriteInfo.substring(0, comma);
						//this.setSprites(index, name, id, media);
					}
				}
			}
			this.actions = new String[5];
			for (int index = 0; index < 5; index++) {
				this.actions[index] = buffer.getString();
				if (this.actions[index].length() == 0) {
					this.actions[index] = null;
				}
			}
		}
		if (this.type == 3) {
			this.filled = buffer.getUByte() == 1;
		}
		if (this.type == 4 || this.type == 1) {
			this.centered = buffer.getUByte() == 1;
			this.fontId = buffer.getUByte();
			//if (fonts != null) {
			//	this.fonts = fonts;
			//	this.font = fonts[this.fontId];
			//}
			this.shadowed = buffer.getUByte() == 1;
		}
		if (this.type == 4) {
			this.disabledText = buffer.getString();
			this.enabledText = buffer.getString();
		}
		if (this.type == 1 || this.type == 3 || this.type == 4) {
			this.disabledColor = buffer.getInt();
		}
		if (this.type == 3 || this.type == 4) {
			this.enabledColor = buffer.getInt();
			this.disabledHoverColor = buffer.getInt();
			this.enabledHoverColor = buffer.getInt();
		}
		if (this.type == 5) {
			String location = buffer.getString();
			if (media != null && location.length() > 0) {
				int comma = location.lastIndexOf(",");
				String name = location.substring(0, comma);
				int id = Integer.parseInt(location.substring(comma + 1));
				//this.setDisabledSprite(name, id, media);
			}
			location = buffer.getString();
			if (media != null && location.length() > 0) {
				int comma = location.lastIndexOf(",");
				String name = location.substring(0, comma);
				int id = Integer.parseInt(location.substring(comma + 1));
				//this.setEnabledSprite(name, id, media);
			}
		}
		if (this.type == 6) {
			int value = buffer.getUByte();
			if (value != 0) {
				this.disabledMediaType = 1;
				this.disabledMediaId = (value - 1 << 8) + buffer.getUByte();
			}
			value = buffer.getUByte();
			if (value != 0) {
				this.enabledMediaType = 1;
				this.enabledMediaId = (value - 1 << 8) + buffer.getUByte();
			}
			value = buffer.getUByte();
			if (value != 0) {
				this.disabledAnimation = (value - 1 << 8) + buffer.getUByte();
			} else {
				this.disabledAnimation = -1;
			}
			value = buffer.getUByte();
			if (value != 0) {
				this.enabledAnimation = (value - 1 << 8) + buffer.getUByte();
			} else {
				this.enabledAnimation = -1;
			}
			this.zoom = buffer.getUShort();
			this.rotationX = buffer.getUShort();
			this.rotationY = buffer.getUShort();
		}
		if (this.type == 7) {
			this.inventory = new int[this.width * this.height];
			this.inventoryAmount = new int[this.width * this.height];
			this.centered = buffer.getUByte() == 1;
			this.fontId = buffer.getUByte();
			//if (fonts != null) {
			//	this.fonts = fonts;
			//	this.font = fonts[this.fontId];
			//}
			this.shadowed = buffer.getUByte() == 1;
			this.disabledColor = buffer.getInt();
			this.invSpritePadX = buffer.getShort();
			this.invSpritePadY = buffer.getShort();
			this.isInventoryInterface = buffer.getUByte() == 1;
			this.actions = new String[5];
			for (int index = 0; index < 5; index++) {
				this.actions[index] = buffer.getString();
				if (this.actions[index].length() == 0) {
					this.actions[index] = null;
				}
			}
		}
		if (this.actionType == 2 || this.type == 2) {
			this.selectedActionName = buffer.getString();
			this.spellName = buffer.getString();
			this.spellUsableOn = buffer.getUShort();
		}
		if (this.type == 8) {
			this.disabledText = buffer.getString();
		}
		if (this.actionType == 1 || this.actionType == 4 || this.actionType == 5 || this.actionType == 6) {
			this.tooltip = buffer.getString();
			if (this.tooltip.length() == 0) {
				if (this.actionType == 1) {
					this.tooltip = "Ok";
				}
				if (this.actionType == 4) {
					this.tooltip = "Select";
				}
				if (this.actionType == 5) {
					this.tooltip = "Select";
				}
				if (this.actionType == 6) {
					this.tooltip = "Continue";
				}
			}
		}
	}

	private void readHeader(Stream stream) {
		id = stream.getUShort();
		if (id == 0xffff) {
			parentId = stream.getUShort();
			id = stream.getUShort();
		}
		Widget rsi = new Widget((WidgetLoader) loader, id);
		this.parentId = parentId;
		this.type = stream.getUByte();
		this.actionType = stream.getUByte();
		this.contentType = stream.getUShort();
		this.width = stream.getUShort();
		this.height = stream.getUShort();
		this.alpha = (byte) stream.getUByte();
		this.hoverId = stream.getUByte();
		if (this.hoverId != 0) {
			this.hoverId = (this.hoverId - 1 << 8) + stream.getUByte();
		} else {
			this.hoverId = -1;
		}
		int requiredmentIndex = stream.getUByte();
		if (requiredmentIndex > 0) {
			this.valueCompareType = new int[requiredmentIndex];
			this.requiredValues = new int[requiredmentIndex];
			for (int index = 0; index < requiredmentIndex; index++) {
				this.valueCompareType[index] = stream.getUByte();
				this.requiredValues[index] = stream.getUShort();
			}
		}
		int valueType = stream.getUByte();
		if (valueType > 0) {
			this.valueIndexArray = new int[valueType][];
			for (int valueIndex = 0; valueIndex < valueType; valueIndex++) {
				int size = stream.getUShort();
				this.valueIndexArray[valueIndex] = new int[size];
				for (int nextIndex = 0; nextIndex < size; nextIndex++) {
					this.valueIndexArray[valueIndex][nextIndex] = stream.getUShort();
				}
			}
		}
	}
}
