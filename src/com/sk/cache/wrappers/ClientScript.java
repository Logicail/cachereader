package com.sk.cache.wrappers;

import com.google.common.base.Joiner;
import com.sk.cache.StringUtil;
import com.sk.cache.wrappers.loaders.ClientScriptLoader;
import com.sk.datastream.Stream;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 23/02/14
 * Time: 19:20
 */
public class ClientScript extends StreamedWrapper {
	public char output; // possibly
	public char input; // possibly
	String defaultString = "null";
	int defaultInt;
	Map map;
	Object[] objects;
	int count = 0;
	HashMap hashMap;

	public ClientScript(ClientScriptLoader loader, int id) {
		super(loader, id);
	}

	public int getCount() {
		return count;
	}

	@Override
	public void decode(Stream stream) {
		int opcode;
		while ((opcode = stream.getUByte()) != 0) {
			if (opcode == 1) {
				input = (char) StringUtil.getCharCode1(stream.getUByte());
			} else {
				if (opcode == 2) {
					output = (char) StringUtil.getCharCode1(stream.getUByte());
				} else {
					if (opcode == 3) {
						defaultString = stream.getString();
					} else {
						if (opcode == 4) {
							defaultInt = stream.getInt();
						} else {
							if (opcode == 5 || opcode == 6) {
								count = stream.getUShort();
								map = new LinkedHashMap(count);
								for (int i = 0; i < count; i++) {
									int x = stream.getInt();
									Serializable serializable;
									if (opcode == 5) {
										serializable = stream.getString();
									} else {
										serializable = stream.getInt();
									}
									map.put(x, serializable);
								}
							} else {
								if (opcode == 7 || opcode == 8) {
									int objectsLength = stream.getUShort();
									count = stream.getUShort();
									objects = new Object[objectsLength];
									for (int i = 0; i < count; i++) {
										int index = stream.getUShort();
										if (opcode == 7) {
											objects[index] = stream.getString();
										} else {
											objects[index] = stream.getInt();
										}
									}
								} else {
									throw new Error(String.format("[%s]Unknown Opcode: %s > %s", getClass().getSimpleName(), opcode, stream));
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		containsKey(0); // so hashMap is built

		sb.append("ClientScript{").append("id=").append(id).append(", input=").append(input).append(" (").append((int) input).append(")").append(", output=").append(output).append(" (").append((int) output).append(")").append(", defaultString='").append(defaultString).append('\'').append(", defaultInt=").append(defaultInt).append(", map=").append(map).append(", objects=").append(Arrays.toString(objects)).append(", count=").append(count);

		if (hashMap == null) {
			sb.append(", hashMap=null");
		} else {
			//sb.append(hashMap);
			sb.append(",\r\n  hashMap={\r\n");
			Joiner joiner = Joiner.on(", ");
			for (Object key : hashMap.keySet()) {
				sb.append("    ").append(key).append("={");
				final int[] list = (int[]) hashMap.get(key);
				ArrayList<Integer> arrayList = new ArrayList<>(list.length);
				for (int integer : list) {
					arrayList.add(integer);
				}
				sb.append(joiner.join(arrayList));
				sb.append("}\r\n");
			}
			sb.append("  }\r\n");
		}

		sb.append('}');
		//sb.append("\r\ntype [" + input + " [" + (int) input + "], " + output + " [" + (int) output + "]]\r\n");

//		final Joiner joiner = Joiner.on(",");
//		containsKey(0); // so hashMap is built
//		switch (output) {
//			case 0:
//				return "Invalid Script " + id;
//			case 's': // String Lookup Table (many to one (index+ => string)
//				sb.append("\r\n");
//				sb.append("type=StringLookupTable\r\n");
//				sb.append("defaultString=").append("\"").append(defaultString).append("\"\r\n");
//				if (hashMap == null) {
//					sb.append("hashMap == null\r\n");
//				} else {
//					for (Object key : hashMap.keySet()) {
//						sb.append("\"").append(key).append("\" => [");
//						List<Integer> listOfIntegers = new ArrayList<>();
//						for (int i : (int[]) hashMap.get(key)) {
//							listOfIntegers.add(i);
//						}
//						sb.append(joiner.join(listOfIntegers));
//						sb.append("]\r\n");
//					}
//				}
//
//				break;
//			case 'z': // ??? only map has a bunch of numbers
//				sb.append("\r\n");
//				sb.append("type=z\r\n");
//				for (Object key : map.keySet()) {
//					sb.append("\"").append(key).append("\" => ");
//					sb.append(map.get(key));
//					sb.append("\r\n");
//				}
//
//				break;
//			default:
//				return "Unknown type [" + input + " [" + (int) input + "], " + output + " [" + (int) output + "]]";
//		}
//
//		sb.append("}");
		return sb.toString();
	}

	public boolean containsKey(Object key) {
		if (count == 0)
			return false;
		if (hashMap == null)
			makeHashMap();
		return hashMap.containsKey(key);
	}

	void makeHashMap() {
		LinkedHashMap hashmap = new LinkedHashMap();
		if (objects != null) {
			for (int i_0_ = 0; i_0_ < objects.length; i_0_++) {
				if (objects[i_0_] != null) {
					Object object = objects[i_0_];
					List list = (List) hashmap.get(object);
					if (list == null) {
						list = new LinkedList();
						hashmap.put(object, list);
					}
					list.add(i_0_);
				}
			}
		} else if (null != map) {
			Iterator iterator = map.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				Object object = entry.getValue();
				List list = (List) hashmap.get(object);
				if (null == list) {
					list = new LinkedList();
					hashmap.put(object, list);
				}
				list.add(entry.getKey());
			}
		} else
			throw new IllegalStateException();
		this.hashMap = new LinkedHashMap();
		Iterator iterator = hashmap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			List list = (List) entry.getValue();
			int[] is = new int[list.size()];
			int i_1_ = 0;
			Iterator iterator_2_ = list.iterator();
			while (iterator_2_.hasNext()) {
				is[i_1_++] = (Integer) iterator_2_.next();
			}
			if (objects == null)
				Arrays.sort(is);
			this.hashMap.put(entry.getKey(), is);
		}
	}

	public Object getInt(int index) {
		Object y = getObject(index);
		if (y == null) {
			return 0;
		} else {
			return y;
		}
	}

	public Object getString(int index) {
		Object y = getObject(index);
		if (y == null) {
			return defaultString;
		} else {
			return y;
		}
	}

	public Object getObject(int index) {
		if (objects != null) {
			if (index < 0 || index >= objects.length) {
				return null;
			} else {
				return objects[index];
			}
		} else {
			if (map != null) {
				return map.get(index);
			} else {
				return null;
			}
		}
	}
}