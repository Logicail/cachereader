package com.sk.cache.wrappers;

import com.sk.cache.wrappers.loaders.ObjectDefinitionLoader;
import com.sk.cache.wrappers.protocol.BasicProtocol;
import com.sk.cache.wrappers.protocol.ExtraAttributeReader;
import com.sk.cache.wrappers.protocol.ProtocolGroup;
import com.sk.cache.wrappers.protocol.StaticLocReader;
import com.sk.cache.wrappers.protocol.extractor.*;
import com.sk.datastream.Stream;
import org.logicail.requirements.Parameter;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ObjectDefinition extends ProtocolWrapper {

	public String name;
	public Integer animationSequence;
	public int type = -1;
	public int width = 1;
	public int height = 1;
	public boolean solid = true;
	public boolean walkable = false;
	public int blockType = 2;
	public String[] actions = new String[5];

	public int scriptId = -1;
	public int configId = -1;
	public int[] childrenIds;

	public int[] modelTypes;
	public int[][] modelIds;

	public String[] modifiedColors;
	public String[] originalColors;
	public int[] modifiedColors2;
	public int[] originalColors2;

	public HashMap<Integer, Object> clientScriptData;
	public HashMap<Parameter, Object> parameters;
	public LinkedHashMap<Integer, Object> unknowns;

	private static String rgb(int i) {
		int hue = (i >>> 10) & 0x3f;
		int saturation = (i >>> 7) & 0x7;
		int brightness = i & 0x7f;

		return hue + "," + saturation + "," + brightness;
	}

	public ObjectDefinition(ObjectDefinitionLoader loader, int id) {
		super(loader, id, protocol);
	}

	private static final ProtocolGroup protocol = new ProtocolGroup();

	static {

		// # 1 {1 ubyte i}
		// [
		//  {1 byte}
		//  ({1 ubyte j}[big_smart]*j)
		// ] *i
		new StaticLocReader(1) {
			@Override
			public void read(Object store, int type, Stream s) {
				int j = s.getUByte();
				int[] modelTypes = new int[j];
				int[][] modelIds = new int[j][];

				for (int k = 0; k < j; k++) {
					modelTypes[k] = s.getByte();
					int d = s.getUByte();
					modelIds[k] = new int[d];
					for (int i = 0; i < d; i++) {
						modelIds[k][i] = s.getBigSmart();
					}
				}

				FieldExtractor.setValue(store, type, type, "modelTypes", modelTypes);
				FieldExtractor.setValue(store, type, type, "modelIds", modelIds);
			}
		}.addSelfToGroup(protocol);

		new StaticLocReader(77, 92) {
			@Override
			public void read(Object obj, int type, Stream s) {
				int script = s.getUShort();
				int config = s.getUShort();
				int ending = type == 92 ? s.getBigSmart() : -1;
				int count = s.getUByte() + 1;
				int[] arr = new int[count + 1];
				for (int i = 0; i < count; ++i)
					arr[i] = s.getBigSmart();
				arr[count] = ending;
				FieldExtractor.setValue(obj, type, type, "scriptId", script == 0xFFFF ? -1 : script);
				FieldExtractor.setValue(obj, type, type, "configId", config == 0xFFFF ? -1 : config);
				FieldExtractor.setValue(obj, type, type, "childrenIds", arr);
			}
		}.addSelfToGroup(protocol);

		new StaticLocReader(40) {
			@Override
			public void read(Object obj, int type, Stream s) {
				int size = s.getUByte();
				String[] modifiedColors = new String[size];
				String[] originalColors = new String[size];

				for (int i = 0; i < size; i++) {
					final int original = s.getUShort();
					final int modified = s.getUShort();
					originalColors[i] = rgb(original);
					modifiedColors[i] = rgb(modified);
				}

				FieldExtractor.setValue(obj, type, type, "modifiedColors", modifiedColors);
				FieldExtractor.setValue(obj, type, type, "originalColors", originalColors);
			}
		}.addSelfToGroup(protocol);

		new StaticLocReader(41) {
			@Override
			public void read(Object obj, int type, Stream s) {
				int size = s.getUByte();
				int[] modifiedColors = new int[size];
				int[] originalColors = new int[size];

				for (int i = 0; size > i; i++) {
					originalColors[i] = s.getUShort();
					modifiedColors[i] = s.getUShort();
				}

				FieldExtractor.setValue(obj, type, type, "modifiedColors2", modifiedColors);
				FieldExtractor.setValue(obj, type, type, "originalColors2", originalColors);
			}
		}.addSelfToGroup(protocol);

		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(new StaticExtractor(null))}, 21, 22, 23, 62, 64, 73, 82, 88, 89, 91, 94, 97, 98, 103, 105, 168, 169, 177, 189, 198, 199, 200).addSelfToGroup(protocol);
		new ExtraAttributeReader().addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.BIG_SMART, "animationSequence")}, 24).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.STRING, "actions")}, 150, 151, 152, 153, 154).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.STRING, "name")}, 2).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new ArrayExtractor(ParseType.UBYTE, 0, new StreamExtractor[]{ParseType.BIG_SMART, ParseType.UBYTE}, null)}, 106).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(new StaticExtractor(1), "blockType")}, 27).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new ArrayExtractor(ParseType.UBYTE, 0, new StreamExtractor[]{ParseType.BYTE}, null)}, 42).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.USHORT), new FieldExtractor(ParseType.USHORT), new FieldExtractor(ParseType.UBYTE), new ArrayExtractor(ParseType.UBYTE, 0, new StreamExtractor[]{ParseType.USHORT}, null)}, 79).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.UBYTE, "height")}, 15).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.USHORT), new FieldExtractor(ParseType.UBYTE)}, 78).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.UBYTE), new FieldExtractor(ParseType.USHORT)}, 99, 100).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(new StaticExtractor(true), "walkable")}, 74).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.BYTE)}, 28, 29, 39, 196, 197).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.USHORT), new FieldExtractor(ParseType.USHORT)}, 173).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(new StaticExtractor(false), "solid"), new FieldExtractor(new StaticExtractor(0), "blockType")}, 17).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.SMART)}, 170, 171).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.UBYTE, "type")}, 19).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new ArrayExtractor(ParseType.UBYTE, 0, new StreamExtractor[]{ParseType.USHORT}, null)}, 5, 160).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.UBYTE, "width")}, 14).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.UBYTE)}, 69, 75, 81, 101, 104, 178, 186, 250, 251, 253, 254).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.STRING, "actions")}, 30, 31, 32, 33, 34).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.USHORT)}, 44, 45, 65, 66, 67, 70, 71, 72, 93, 95, 102, 107, 164, 165, 166, 167, 190, 191, 192, 193, 194, 195).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.INT)}, 162).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(new StaticExtractor(false), "solid")}, 18).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.BYTE), new FieldExtractor(ParseType.BYTE), new FieldExtractor(ParseType.BYTE), new FieldExtractor(ParseType.BYTE)}, 163).addSelfToGroup(protocol);
		//new BasicProtocol(new FieldExtractor[]{new ArrayExtractor(ParseType.UBYTE, 0, new StreamExtractor[]{ParseType.USHORT, ParseType.USHORT}, null)}, /*40,*/ 41).addSelfToGroup(protocol);
		new BasicProtocol(new FieldExtractor[]{new FieldExtractor(ParseType.USHORT), new FieldExtractor(ParseType.USHORT), new FieldExtractor(ParseType.USHORT)}, 252, 255).addSelfToGroup(protocol);

	}

}
