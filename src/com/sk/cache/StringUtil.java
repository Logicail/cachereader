package com.sk.cache;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 23/02/14
 * Time: 19:48
 */
public class StringUtil {
	private static int k = (int) "?".charAt(0);
	private static int q = (int) "A".charAt(0);
	private static int r = (int) "a".charAt(0);
	private static int i = (int) "F".charAt(0);
	private static int c = (int) "f".charAt(0);
	private static int u = (int) "Z".charAt(0);
	private static int j = (int) "z".charAt(0);
	private static int o = (int) "0".charAt(0);
	private static int g = (int) "9".charAt(0);
	private static int[] n = new int[]{(int) "\u20ac".charAt(0), 0, (int) "\u201a".charAt(0), (int) "\u0192".charAt(0), (int) "\u201e".charAt(0), (int) "\u2026".charAt(0), (int) "\u2020".charAt(0), (int) "\u2021".charAt(0), (int) "\u02c6".charAt(0), (int) "\u2030".charAt(0), (int) "\u0160".charAt(0), (int) "\u2039".charAt(0), (int) "\u0152".charAt(0), 0, (int) "\u017d".charAt(0), 0, 0, (int) "\u2018".charAt(0), (int) "\u2019".charAt(0), (int) "\u201c".charAt(0), (int) "\u201d".charAt(0), (int) "\u2022".charAt(0), (int) "\u2013".charAt(0), (int) "\u2014".charAt(0), (int) "\u02dc".charAt(0), (int) "\u2122".charAt(0), (int) "\u0161".charAt(0), (int) "\u203a".charAt(0), (int) "\u0153".charAt(0), 0, (int) "\u017e".charAt(0), (int) "\u0178".charAt(0)};


	private static HashMap<Integer, Integer> t = new HashMap<Integer, Integer>();

	static {
		t.put((int) "\u20ac".charAt(0), 128);
		t.put((int) "\u201a".charAt(0), 130);
		t.put((int) "\u0192".charAt(0), 131);
		t.put((int) "\u201e".charAt(0), 132);
		t.put((int) "\u2026".charAt(0), 133);
		t.put((int) "\u2020".charAt(0), 134);
		t.put((int) "\u2021".charAt(0), 135);
		t.put((int) "\u02c6".charAt(0), 136);
		t.put((int) "\u2030".charAt(0), 137);
		t.put((int) "\u0160".charAt(0), 138);
		t.put((int) "\u2039".charAt(0), 139);
		t.put((int) "\u0152".charAt(0), 140);
		t.put((int) "\u017d".charAt(0), 142);
		t.put((int) "\u2018".charAt(0), 145);
		t.put((int) "\u2019".charAt(0), 146);
		t.put((int) "\u201c".charAt(0), 147);
		t.put((int) "\u201d".charAt(0), 148);
		t.put((int) "\u2022".charAt(0), 149);
		t.put((int) "\u2013".charAt(0), 150);
		t.put((int) "\u2014".charAt(0), 151);
		t.put((int) "\u02dc".charAt(0), 152);
		t.put((int) "\u2122".charAt(0), 153);
		t.put((int) "\u0161".charAt(0), 154);
		t.put((int) "\u203a".charAt(0), 155);
		t.put((int) "\u0153".charAt(0), 156);
		t.put((int) "\u017e".charAt(0), 158);
		t.put((int) "\u0178".charAt(0), 159);
	}

	public static boolean checkCharCode(int v) {
		v = v & 255;
		if (v >= 128 && v < 160) {
			if (n[v - 128] == 0) {
				return false;
			}
		}
		return true;
	}

	public static int getCharCode(int v) {
		int x = -1;
		if ((v > 0 && v < 128) || (v >= 160 && v <= 255)) {
			x = v;
		} else {
			x = t.get(v);
		}
		if (x == -1) {
			x = k;
		}
		return x;
	}

	public static int getCharCode1(int v) {
		int x = v & 0xff;
		if (x == 0) {
			throw new Error("881 " + v);
		}
		if (x >= 128 && x < 160) {
			int y = n[x - 128];
			if (y == 0) {
				y = k;
			}
			x = y;
		}
		return x;
	}

	public static String formatString(String v) {
		int x = v.length();
		char[] y = new char[x];
		for (int z = 0; z < x; z++) {
			int A = v.charAt(z);
			if ((A > 0 && A < 128) || (A >= 160 && A <= 255)) {
				y[z] = (char) A;
				continue;
			}
			y[z] = (char) t.get(A).intValue();
			if (y[z] == 0) {
				y[z] = (char) k;
			}
		}
		return String.copyValueOf(y);
	}

	public static String formatString1(String x) {
		int v = x.length();
		char[] y = new char[v];
		int C = 0;
		for (int A = 0; A < v; A++) {
			int z = x.charAt(A) & 255;
			if (z == 0) {
				continue;
			}
			if (z >= 128 && z < 160) {
				int B = n[z - 128];
				if (B == 0) {
					B = k;
				}
				z = B;
			}
			y[C++] = (char) z;
		}
		return String.copyValueOf(y, 0, C);
	}

	public static String formatString2(String B) {
		String y = "";
		char x;
		int A;
		int v;
		int C;
		for (int z = 0; z < B.length(); z++) {
			x = B.charAt(z);
			A = x;
			if ((A >= r && A <= j) || (A >= q && A <= u) || x == ".".charAt(0) || x == "-".charAt(0) || x == "*".charAt(0) || x == "_".charAt(0)) {
				y += x;
			} else {
				if (x == " ".charAt(0)) {
					y += "+";
				} else {
					v = formatString(String.valueOf(x)).charAt(0);
					y += "%";
					C = (v >> 4) & 15;
					if (C >= 10) {
						y += (char) (q - 10 + C);
					} else {
						y += (char) (o + C);
					}
					C = v & 15;
					if (C >= 10) {
						y += (char) (q - 10 + C);
					} else {
						y += (char) (o + C);
					}
				}
			}
		}
		return y;
	}

	public static String formatString3(String C) {
		String x = "";
		int v;
		int B;
		int A;
		int y = C.length();
		for (int z = 0; z < y; z++) {
			v = C.charAt(z);
			if (v == "%".charAt(0) && y > z + 2) {
				v = C.charAt(z + 1);
				B = v;
				A = 0;
				if (B >= r && B <= c) {
					A = 10 + B - r;
				} else {
					if (B >= q && B <= i) {
						A = 10 + B - q;
					} else {
						if (B >= o && B <= g) {
							A = B - o;
						} else {
							x += "%";
							continue;
						}
					}
				}
				B = B << 4;
				v = C.charAt(z + 2);
				B = v;
				if (B >= r && B <= c) {
					A += 10 + B - r;
				} else {
					if (B >= q && B <= i) {
						A += 10 + B - q;
					} else {
						if (B >= o && B <= g) {
							A += B - o;
						} else {
							x += "%";
							continue;
						}
					}
				}
				if (A != 0 && checkCharCode(A)) {
					x += (char) (getCharCode1(A));
				}
			} else {
				if (v == "+".charAt(0)) {
					x += " ";
				} else {
					x += (char) v;
				}
			}
		}
		return x;
	}

}
