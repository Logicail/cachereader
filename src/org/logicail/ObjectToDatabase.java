package org.logicail;

import com.google.common.base.Joiner;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.wrappers.NpcDefinition;
import com.sk.cache.wrappers.ObjectDefinition;
import com.sk.cache.wrappers.Script;
import com.sk.cache.wrappers.loaders.NpcDefinitionLoader;
import com.sk.cache.wrappers.loaders.ObjectDefinitionLoader;
import com.sk.cache.wrappers.loaders.RegionLoader;
import com.sk.cache.wrappers.loaders.ScriptLoader;
import com.sk.cache.wrappers.region.LocalObject;
import com.sk.cache.wrappers.region.Region;
import org.apache.commons.lang.ArrayUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 24/03/2014
 * Time: 17:39
 */
public class ObjectToDatabase {
	public static void main(String[] args) throws FileNotFoundException {
		CacheSystem cache = new CacheSystem(new File(System.getProperty("user.home"), File.separator + "jagexcache" + File.separator + "runescape" + File.separator + "LIVE"));
		try {
			//scriptsToDatabase(cache);
			//objectsToDatabase(cache);
			regionToDatabase(cache);
			//npcToDatabase(cache);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void npcToDatabase(CacheSystem cache) throws SQLException, ClassNotFoundException {
		NpcDefinitionLoader loader = new NpcDefinitionLoader(cache);

		final Connection connection = connect();

		connection.createStatement().executeUpdate("DELETE FROM [dbo].[NpcDefinition]");

		final Joiner joiner = Joiner.on(",").skipNulls();
		String sql = "INSERT INTO [dbo].[NpcDefinition] ([id],[name],[combatLevel],[clickable],[visible],[actions],[scriptid],[configid],[childrenids],[models],[additionalModels]) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		for (int id = 0; id < 0xfffff; id++) {
			if (!loader.canLoad(id)) {
				continue;
			}
			try {
				final NpcDefinition definition = loader.load(id);

				final Integer[] integers = definition.childrenIds != null ? ArrayUtils.toObject(definition.childrenIds) : null;
				preparedStatement.setInt(1, definition.getId());
				preparedStatement.setString(2, definition.name);
				preparedStatement.setInt(3, definition.combatLevel);
				preparedStatement.setBoolean(4, definition.clickable);
				preparedStatement.setBoolean(5, definition.visible);
				final String join = definition.actions != null ? joiner.join(definition.actions) : null;
				preparedStatement.setObject(6, (join != null && !join.isEmpty() ? join : null));
				preparedStatement.setObject(7, definition.scriptId > -1 ? definition.scriptId : null);
				preparedStatement.setObject(8, definition.configId > -1 ? definition.configId : null);
				preparedStatement.setObject(9, (integers != null ? joiner.join(integers) : null));

				final int[] ints = definition.modelIds != null ? definition.modelIds : null;
				if (ints != null) {
					final List<Integer> list = Arrays.asList(ArrayUtils.toObject(ints));
					Collections.sort(list);
					preparedStatement.setObject(10, list.toString());
				} else {
					preparedStatement.setObject(10, null);
				}
				final int[] additional = definition.additionalModels != null ? definition.additionalModels : null;
				if (additional != null) {
					final List<Integer> list = Arrays.asList(ArrayUtils.toObject(additional));
					Collections.sort(list);
					preparedStatement.setObject(11, list.toString());
				} else {
					preparedStatement.setObject(11, null);
				}

				preparedStatement.addBatch();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		preparedStatement.executeBatch();
	}

	public static void scriptsToDatabase(CacheSystem cacheSystem) throws SQLException, ClassNotFoundException {
		ScriptLoader loader = new ScriptLoader(cacheSystem);

		final Connection connection = connect();

		connection.createStatement().executeUpdate("DELETE FROM [dbo].[Script]");

		String sql = "INSERT INTO [dbo].[Script] ([id],[configId],[shift],[mask]) VALUES (?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		int id = 0;
		while (loader.canLoad(id)) {
			final Script script = loader.load(id);

			preparedStatement.setInt(1, script.getId());
			preparedStatement.setInt(2, script.configId);
			preparedStatement.setInt(3, script.lowerBitIndex);
			int mask = 0;
			int upper = script.upperBitIndex - script.lowerBitIndex;
			for (int i = 0; i <= upper; ++i) {
				mask += Math.pow(2, i);
			}
			preparedStatement.setInt(4, mask);
			preparedStatement.addBatch();
			id++;
		}
		preparedStatement.executeBatch();
	}

	public static void objectsToDatabase(CacheSystem cacheSystem) throws SQLException, ClassNotFoundException {
		ObjectDefinitionLoader loader = new ObjectDefinitionLoader(cacheSystem);

		final Connection connection = connect();

		connection.createStatement().executeUpdate("DELETE FROM [dbo].[ObjectDefinition]");

		final Joiner joiner = Joiner.on(",").skipNulls();
		String sql = "INSERT INTO [dbo].[ObjectDefinition] ([id],[name],[type],[width],[height],[solid],[walkable],[blocktype],[actions],[scriptid],[configid],[childrenids],[models]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		int id = 0;
		while (loader.canLoad(id)) {
			final ObjectDefinition definition = loader.load(id);

			final Integer[] integers = definition.childrenIds != null ? ArrayUtils.toObject(definition.childrenIds) : null;
			preparedStatement.setInt(1, definition.getId());
			preparedStatement.setString(2, definition.name);
			preparedStatement.setInt(3, definition.type);
			preparedStatement.setInt(4, definition.width);
			preparedStatement.setInt(5, definition.height);
			preparedStatement.setBoolean(6, definition.solid);
			preparedStatement.setBoolean(7, definition.walkable);
			preparedStatement.setInt(8, definition.blockType);
			final String join = definition.actions != null ? joiner.join(definition.actions) : null;
			preparedStatement.setObject(9, (join != null && !join.isEmpty() ? join : null));
			preparedStatement.setObject(10, definition.scriptId > -1 ? definition.scriptId : null);
			preparedStatement.setObject(11, definition.configId > -1 ? definition.configId : null);
			preparedStatement.setObject(12, (integers != null ? joiner.join(integers) : null));

			final int[] ints = definition.modelIds != null ? definition.modelIds[0] : null;
			if (ints != null) {
				final List<Integer> list = Arrays.asList(ArrayUtils.toObject(ints));
				Collections.sort(list);
				preparedStatement.setObject(13, list.toString());
			} else {
				preparedStatement.setObject(13, null);
			}
			preparedStatement.addBatch();
			id++;
		}
		preparedStatement.executeBatch();
	}

	public static void regionToDatabase(CacheSystem cacheSystem) throws SQLException, ClassNotFoundException {
		RegionLoader loader = new RegionLoader(cacheSystem); // for this could have just used a LocalObjectLoader

		final Connection connection = connect();

		connection.createStatement().executeUpdate("DELETE FROM [dbo].[RegionObjects]");

		String sql = "INSERT INTO [dbo].[RegionObjects] ([regionX],[regionY],[x],[y],[floor],[cartesian],[objectId],[type],[orientation],[other]) VALUES (?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		// This takes a long time (because of sql query)
		for (Region definition : loader) {
			//System.out.println(definition.x + " " + definition.y);
			int offsetX = definition.x * 64;
			int offsetY = definition.y * 64;

			for (LocalObject object : definition.objects.getObjects()) {
				preparedStatement.setObject(1, definition.x);
				preparedStatement.setObject(2, definition.y);
				preparedStatement.setObject(3, offsetX + object.x);
				preparedStatement.setObject(4, offsetY + object.y);
				preparedStatement.setObject(5, object.plane);
				preparedStatement.setString(6, "POINT(" + (offsetX + object.x) + " " + (offsetY + object.y) + ")");
				preparedStatement.setObject(7, object.id);
				preparedStatement.setObject(8, object.type);
				preparedStatement.setObject(9, object.orientation);
				preparedStatement.setObject(10, object.meta);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		}
	}

	public static java.sql.Connection connect() throws ClassNotFoundException, SQLException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String url = "jdbc:sqlserver://localhost;databaseName=Runescape;integratedSecurity=true";
		return DriverManager.getConnection(url);
	}
}
