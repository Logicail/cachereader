package org.logicail;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.google.common.base.Joiner;
import com.sk.cache.dist.pack.RegionPacker;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.fs.CacheType;
import com.sk.cache.wrappers.*;
import com.sk.cache.wrappers.loaders.*;
import org.apache.commons.lang.ArrayUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 23/02/14
 * Time: 16:02
 */
public class Dumper {
	public static void main(String[] args) throws IOException {
		//C:\Users\Michael\jagexcache\oldschool\LIVE
		CacheSystem cache = new CacheSystem(new File(System.getProperty("user.home"), File.separator + "jagexcache" + File.separator + "runescape" + File.separator + "LIVE"));
		//dumpScripts(cache);
		//dumpScriptsById(cache);
		//dumpClientScripts(cache);
		//dumpCsMap(cache);
		//dumpItemMap(cache);
		//dumpTradeable(cache);
		//pack(cache, "packed/PackedRegion.packed");
		//dumpItems(cache);
		//dumpDynamicNpcs(cache);
		//dumpObjects(cache);
		dumpDynamicObjects(cache);
		//dumpGeItems(cache);
		//category(cache);
		//dumpImages(cache);
		//dumpNpcs(cache);
		//dumpQuests(cache);
	}

	private static void dumpQuests(CacheSystem cache) {
		QuestDefinitionLoader loader = new QuestDefinitionLoader(cache);
		int id = 0;
		try (PrintWriter writer = new PrintWriter(new File("quests.txt"))) {
			while (loader.canLoad(id)) {
				final QuestDefinition definition = loader.load(id);
				writer.println(definition.toString());
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpNpcs(CacheSystem cache) {
		NpcDefinitionLoader loader = new NpcDefinitionLoader(cache);
		int id = 0;
		try (PrintWriter writer = new PrintWriter(new File("npcs.txt"))) {
			while (loader.canLoad(id)) {
				final NpcDefinition definition = loader.load(id);
				writer.println(definition.toString());
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpImages(CacheSystem cache) throws IOException {
		File dir = new File("../images");
		if (!dir.exists()) {
			dir.mkdir();
		}

		ImageLoader loader = new ImageLoader(cache);
		int id = 0;
		while (loader.canLoad(id)) {
			final CacheImage load = loader.load(id);
			int x = 0;
			for (CacheImage.ImageData image : load.images) {
				final BufferedImage bufferedImage = image.getImage();
				if (bufferedImage != null) {
					File outputfile = new File(dir.getAbsolutePath(), id + "-" + x + ".png");
					ImageIO.write(bufferedImage, "png", outputfile);
				}
				x++;
			}
			id++;
		}
	}

	private static void dumpDynamicNpcs(CacheSystem cache) {
		NpcDefinitionLoader loader = new NpcDefinitionLoader(cache);
		ScriptLoader scriptLoader = new ScriptLoader(cache);
		int id = 0;
		try (PrintWriter writer = new PrintWriter(new File("dynamic_npcs.java"))) {
			while (loader.canLoad(id)) {
				final NpcDefinition definition = loader.load(id);
				if (definition.childrenIds != null) {
					writer.println(definition);
					int mask = definition.childrenIds.length;
					if (definition.configId != -1) {
						writer.println("switch (ctx.varpbits.varpbit(" + definition.configId + ")) {");
					} else {
						final Script script = scriptLoader.load(definition.scriptId);
						mask = script.mask();
						writer.println("switch (" + prettyScript(script) + ") {");
					}
					int[] childrenIds = definition.childrenIds;
					for (int i = 0; i < childrenIds.length; i++) {
						if (i > mask) {
							break;
						}

						int childrenId = childrenIds[i];
						final NpcDefinition childDefinition = loader.canLoad(childrenId) ? loader.load(childrenId) : null;
						writer.print("\tcase " + i + ": \t " + childrenId + " " + (childDefinition == null ? "null" : childDefinition));
						//writer.print("\tcase " + i + ": \t " + childrenId + " " + (childDefinition == null ? "null" : "\"" + childDefinition.name + "\" \"" + joiner.join(childDefinition.actions) + "\""));

						writer.println();
					}

					writer.println("}");
					writer.println();
				}
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpTradeable(CacheSystem cache) {
		ItemDefinitionLoader loader = new ItemDefinitionLoader(cache);
		try (PrintWriter out = new PrintWriter(new FileOutputStream("tradable.json"))) {
			JsonObject json = new JsonObject();
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			json.set("time", dateFormatGmt.parse(dateFormatGmt.format(new Date())).toString());

			JsonArray tradable = new JsonArray();

			int id = 1;
			while (loader.canLoad(id)) {
				final ItemDefinition load = loader.load(id);
				if (!load.noted && !load.lent && load.tradable != null && load.tradable) {
					tradable.add(id);
				}
				id++;
			}

			json.set("tradable", tradable);

			json.writeTo(out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static void category(CacheSystem cache) {
		CategoryLoader loader = new CategoryLoader(cache);
		final CategoryDefinition load = loader.load(0);
		System.out.println(load);
	}

	private static void dumpGeItems(CacheSystem cache) {
		ItemDefinitionLoader loader = new ItemDefinitionLoader(cache);
		int id = 1;
		try (PrintWriter writer = new PrintWriter(new File("ge.txt"))) {
			while (loader.canLoad(id)) {
				//if (id == 2 || id == 4) {
				final ItemDefinition definition = loader.load(id);
				if (/*!definition.noted && !definition.lent &&*/ definition.tradable) {
					writer.println(definition.toString());
				}
				//}
				//if (id > 4) {
				//	break;
				//}
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpDynamicObjects(CacheSystem cache) {
		ObjectDefinitionLoader loader = new ObjectDefinitionLoader(cache);
		ScriptLoader scriptLoader = new ScriptLoader(cache);
		final Joiner joiner = Joiner.on(", ").skipNulls();
		int id = 0;
		try (PrintWriter writer = new PrintWriter(new File("dynamic objects.java"))) {
			while (loader.canLoad(id)) {
				final ObjectDefinition definition = loader.load(id);
				if (definition.scriptId != -1) {
					writer.println(definition);
					int mask = definition.childrenIds.length;
					if (definition.configId != -1) {
						writer.println("switch (ctx.varpbits.varpbit(" + definition.configId + ")) {");
					} else {
						final Script script = scriptLoader.load(definition.scriptId);
						mask = script.mask();
						writer.println("switch (" + prettyScript(script) + ") {");
					}

					int[] childrenIds = definition.childrenIds;
					for (int i = 0; i < childrenIds.length; i++) {
						if (i > mask) {
							break;
						}

						int childrenId = childrenIds[i];
						final ObjectDefinition childDefinition = loader.canLoad(childrenId) ? loader.load(childrenId) : null;
						writer.print("\tcase " + i + ": \t " + childrenId + " " + (childDefinition == null ? "null" : "\"" + childDefinition.name + "\" \"" + joiner.join(childDefinition.actions) + "\""));


						if (childDefinition != null && childDefinition.modelIds != null) {
							/*
							OBJECT TYPES:

							0	- straight walls, fences etc
							1	- diagonal walls corner, fences etc connectors
							2	- entire walls, fences etc corners
							3	- straight wall corners, fences etc connectors
							4	- straight inside wall decoration
							5	- straight outside wall decoration
							6	- diagonal outside wall decoration
							7	- diagonal inside wall decoration
							8	- diagonal in wall decoration
							9	- diagonal walls, fences etc
							10	- all kinds of objects, trees, statues, signs, fountains etc etc
							11	- ground objects like daisies etc
							12	- straight sloped roofs
							13	- diagonal sloped roofs
							14	- diagonal slope connecting roofs
							15	- straight sloped corner connecting roofs
							16	- straight sloped corner roof
							17	- straight flat top roofs
							18	- straight bottom egde roofs
							19	- diagonal bottom edge connecting roofs
							20	- straight bottom edge connecting roofs
							21	- straight bottom edge connecting corner roofs
							22	- ground decoration + map signs (quests, water fountains, shops etc)
							 */
							//writer.print(" modelsTypes: " + Arrays.toString(childDefinition._wml)); // ?
							writer.print(" ");
							for (int[] modelId : childDefinition.modelIds) {
								writer.print(" \"" + joiner.join(ArrayUtils.toObject(modelId)) + "\"");
							}
						}

//						if (childDefinition != null && childDefinition.originalColors != null && childDefinition.modifiedColors != null) {
//							writer.print(" \"" + joiner.join(childDefinition.originalColors) + "\"");
//							writer.print(" \"" + joiner.join(childDefinition.modifiedColors) + "\"");
//						}

						writer.println();
					}
					writer.println("}");
					writer.println();
				}
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpObjects(CacheSystem cache) {
		ObjectDefinitionLoader loader = new ObjectDefinitionLoader(cache);
		int id = 0;
		try (PrintWriter writer = new PrintWriter(new File("objects.txt"))) {
			while (loader.canLoad(id)) {
				final ObjectDefinition definition = loader.load(id);
				writer.println(definition.toString());
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpItems(CacheSystem cache) {
		ItemDefinitionLoader loader = new ItemDefinitionLoader(cache);
		int id = 0;
		try (PrintWriter writer = new PrintWriter(new File("items.txt"))) {
			while (loader.canLoad(id)) {
				final ItemDefinition definition = loader.load(id);
				writer.println(definition.toString());
				id++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void pack(CacheSystem cache, String path) {
		RegionPacker packer = new RegionPacker(new RegionLoader(cache));
		try (FileOutputStream stream = new FileOutputStream(path)) {
			packer.pack(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void dumpItemMap(CacheSystem cache) {
		ItemMapping mapping = new ItemMapping(cache);
		try (PrintWriter out = new PrintWriter(new FileOutputStream("itemmapping.json"))) {
			JsonObject json = new JsonObject();
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
			json.set("time", dateFormatGmt.parse(dateFormatGmt.format(new Date())).toString());
			mapping.write(json);
			json.writeTo(out);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static void dumpCsMap(CacheSystem cache) {
		ClientScriptLoader loader = new ClientScriptLoader(cache);
		final CacheType cacheType = cache.getCacheSource().getCacheType(17);
		int scriptId = 0;
		while (loader.canLoad(scriptId)) {
			final byte[] data = cacheType.getArchive(scriptId >>> 8).getFile(scriptId & 0xff).getData();
			if (data != null && data.length > 1) {
				try (FileOutputStream fos = new FileOutputStream("scripts/dat/" + scriptId + ".dat")) {
					fos.write(data);
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			scriptId++;
		}
	}

	private static void dumpClientScripts(CacheSystem cache) {
		ClientScriptLoader loader = new ClientScriptLoader(cache);
		try (PrintWriter all = new PrintWriter(new File("clientscripts.txt"))) {
			int scriptId = 0;
			while (loader.canLoad(scriptId)) {
				StringBuilder sb = new StringBuilder();
				ClientScript script = loader.load(scriptId);
				sb.append(script);
				if (script.getCount() > 0) {
					//sb.append(script);
					all.write(script.toString());
					all.write("\r\n");
					all.println();
				}
				if (sb.length() > 0) {
					try (PrintWriter writer = new PrintWriter(new File("scripts", scriptId + ".cs"))) {
						//System.out.println(sb.toString());
						writer.write(sb.toString());
						writer.println();
						writer.println();
						//writer.write(script.cs2());
						if (script != null) {
							//writer.write(script.cs2());
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}

				scriptId++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpScripts(CacheSystem cache) {
		ScriptLoader loader = new ScriptLoader(cache);

		try (PrintWriter writer = new PrintWriter(new File("scripts.txt"))) {
			int scriptId = 0;

			while (loader.canLoad(scriptId)) {
				final Script script = loader.load(scriptId);
				writer.println("[" + script.getId() + "] " + prettyScript(script));
				scriptId++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void dumpScriptsById(CacheSystem cache) {
		ScriptLoader loader = new ScriptLoader(cache);
		TreeMap<Integer, List<Script>> map = new TreeMap<>(); // Just so don't have to sort keys

		int scriptId = 0;
		while (loader.canLoad(scriptId)) {
			try {
				final Script script = loader.load(scriptId);
				List<Script> list;
				if (!map.containsKey(script.configId)) {
					list = new LinkedList<>();
					map.put(script.configId, list);
				}
				list = map.get(script.configId);
				list.add(script);
			} catch (Exception ignored) {
			}
			scriptId++;
		}

		try (PrintWriter txt = new PrintWriter(new FileWriter(new File("scriptsById.txt")))) {
			try (BufferedWriter json = new BufferedWriter(new FileWriter(new File("scriptsById.json")))) {
				JsonObject root = new JsonObject();

				for (Integer key : map.keySet()) {
					JsonArray setting = new JsonArray();
					txt.println("[id=" + key + "]");

					final List<Script> scripts = map.get(key);
					for (Script script : scripts) {
						JsonObject value = new JsonObject();

						int mask = 0;
						int upper = script.upperBitIndex - script.lowerBitIndex;
						for (int i = 0; i <= upper; ++i) {
							mask += Math.pow(2, i);
						}

						// may want to add script.getId() - higher values are newer and more likely to still be used
						value.set("shift", script.lowerBitIndex);
						value.set("mask", mask);

						txt.println("  [" + script.getId() + "] ctx.varpbits.varpbit(" + script.configId + ", " + script.lowerBitIndex + ", 0x" + Integer.toHexString(mask) + ")");

						setting.add(value);
					}
					txt.println("");

					root.set(key + "", setting);
				}

				root.writeTo(json);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String prettyScript(Script script) {
		int mask = 0;
		int upper = script.upperBitIndex - script.lowerBitIndex;
		for (int i = 0; i <= upper; ++i) {
			mask += Math.pow(2, i);
		}
		return "ctx.varpbits.varpbit(" + script.configId + ", " + script.lowerBitIndex + ", 0x" + Integer.toHexString(mask) + ")";
	}
}
