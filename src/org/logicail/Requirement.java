package org.logicail;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 02/03/14
 * Time: 20:44
 */
public interface Requirement<T> {
	public T get(Map params);
}
