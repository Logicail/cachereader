package org.logicail;

import com.sk.cache.wrappers.loaders.RegionLoader;
import com.sk.cache.wrappers.region.Region;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 03/09/2014
 * Time: 18:52
 */
public class RegionIterator implements Iterator<Region> {
	private final RegionLoader loader;
	private final Deque<Integer> deque;

	public RegionIterator(RegionLoader loader) {
		this.loader = loader;
		deque = new ArrayDeque<>(loader.validRegionHashes());
	}

	@Override
	public boolean hasNext() {
		return !deque.isEmpty();
	}

	@Override
	public Region next() {
		return loader.load(deque.remove());
	}

	@Override
	public void remove() {
		deque.remove();
	}
}
