package org.logicail;

import com.eclipsesource.json.JsonObject;
import com.sk.cache.fs.CacheSystem;
import com.sk.cache.wrappers.ItemDefinition;
import com.sk.cache.wrappers.loaders.WrapperLoader;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 02/03/14
 * Time: 15:44
 */
public class ItemMapping {
	private static Map<Integer, Integer> map = new LinkedHashMap<>();

	public static int getId(int id) {
		return map.containsKey(id) ? map.get(id) : id;
	}

	/// Map Noted => Real id
	public ItemMapping(CacheSystem cache) {
		final WrapperLoader<ItemDefinition> loader = cache.getLoader(ItemDefinition.class);
		// map of items ids to real id
		//try (PrintWriter items = new PrintWriter(new File("items.txt"))) {
		for (int id = 0; id < 0xfffff; id++) {
			if (loader.canLoad(id)) {
				final ItemDefinition definition = loader.load(id);
				//if (definition.clientScriptData != null) {
				//items.println(definition);
				//}
				if (definition.lent) {
					map.put(id, definition.lentId);
				} else if (definition.noted) {
					map.put(id, definition.noteId);
				} else if (definition.cosmetic) {
					map.put(id, definition.cosmeticId);
				}
				// Exclude when id == id
				//map.put(definition.noteId, id);
			}
		}
		//} catch (FileNotFoundException e) {
		//	e.printStackTrace();
		//}
	}

	public void write(JsonObject out) {
		JsonObject mapping = new JsonObject();

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			mapping.add(entry.getKey() + "", entry.getValue());
		}

		out.add("mapping", mapping);
	}
}
