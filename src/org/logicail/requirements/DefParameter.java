package org.logicail.requirements;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 02/03/14
 * Time: 20:42
 */
public interface DefParameter<T> {
	public T get(Map params);
}
