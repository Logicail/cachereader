package org.logicail.requirements;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import org.logicail.ItemMapping;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 02/03/14
 * Time: 14:18
 */
public class RSApi {
	private static final Map<Integer, RSApiItem> cache = new ConcurrentHashMap<Integer, RSApiItem>();

	public static synchronized Map<Integer, RSApiItem> getProfiles(int... ids) {
		HashSet<Integer> set = new HashSet<Integer>();
		for (int id : ids) {
			set.add(ItemMapping.getId(id));
		}

		return getProfilesInternal(set);
	}

	private static synchronized Map<Integer, RSApiItem> getProfilesInternal(HashSet<Integer> set) {
		Map<Integer, RSApiItem> result = new HashMap<Integer, RSApiItem>(set.size());

		HashSet<Integer> lookup = new HashSet<Integer>();
		for (int id : set) {
			if (cache.containsKey(id)) {
				result.put(id, cache.get(id));
			} else {
				lookup.add(id);
			}
		}

		List<Integer> list = new ArrayList<Integer>(lookup);

		if (list.size() == 0) {
			return result;
		}

		int partitionSize = 10;
		List<List<Integer>> partitions = new LinkedList<List<Integer>>();
		for (int i = 0; i < list.size(); i += partitionSize) {
			partitions.add(list.subList(i, i + Math.min(partitionSize, list.size() - i)));
		}

		for (List<Integer> partition : partitions) {
			try {
				final String data = IOUtil.readString(getUrl(partition).toString(), "LogBankOrganiser");
				final JsonArray json = JsonArray.readFrom(data);

				for (JsonValue value : json) {
					final JsonObject object = value.asObject();
					final RSApiItem item = new RSApiItem(
							object.get("id").asInt(),
							new URL(object.get("icon").asString()),
							new URL(object.get("icon_large").asString()),
							object.get("type").asString(),
							object.get("name").asString(),
							object.get("description").asString(),
							object.get("membersitem").asString().equals("true"),
							Integer.parseInt(object.get("prices").asObject().get("exact").asString()));
					result.put(item.getId(), item);
				}
			} catch (MalformedURLException ignored) {
				ignored.printStackTrace();
			}
		}

		return result;
	}

	public static void main(String[] args) {
		final Map<Integer, RSApiItem> profile = RSApi.getProfiles(163, 2436, 15308, 15312, 3024, 15305, 15304, 15306, 21630, 21632, 2456, 2442, 23495, 23501, 15309, 15313, 15321, 3018, 15300, 15332, 15333, 2448, 3432, 3436, 15334, 2434, 107, 15311, 15325, 15324, 555, 565, 559, 562, 560);
		System.out.println(profile.size());
	}

	private static URL getUrl(List<Integer> ids) throws MalformedURLException {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ids.size(); i++) {
			Integer id = ids.get(i);
			sb.append(id);
			if (i != ids.size() - 1) {
				sb.append(",");
			}
		}
		return new URL("http://api.rsapi.org/ge/item/" + sb.toString() + ".json");
	}
}
