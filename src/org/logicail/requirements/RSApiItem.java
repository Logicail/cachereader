package org.logicail.requirements;

import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 02/03/14
 * Time: 19:06
 */
public class RSApiItem {
	private final int id;
	private final URL icon;
	private final URL icon_large;
	private final String type;
	private final String name;
	private final String description;
	private final boolean members;
	private final int price;

	public int getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isMembers() {
		return members;
	}

	public int getPrice() {
		return price;
	}

	public RSApiItem(int id, URL icon, URL icon_large, String type, String name, String description, boolean members, int price) {
		this.id = id;
		this.icon = icon;
		this.icon_large = icon_large;
		this.type = type;
		this.name = name;
		this.description = description;
		this.members = members;
		this.price = price;
	}

	@Override
	public String toString() {
		return "RSApiItem{" +
				"price=" + price +
				", id=" + id +
				", name='" + name + '\'' +
				", type='" + type + '\'' +
				", icon=" + icon +
				", icon_large=" + icon_large +
				", description='" + description + '\'' +
				", members=" + members +
				'}';
	}
}
