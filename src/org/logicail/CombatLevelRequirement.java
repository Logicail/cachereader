package org.logicail;

import org.logicail.requirements.SkillRequirement;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 02/03/14
 * Time: 20:47
 */
public class CombatLevelRequirement extends SkillRequirement {

	public CombatLevelRequirement(int level) {
		super(-1, level);
	}

	@Override
	public CombatLevelRequirement get(Map params) {
		return null;
	}

	@Override
	public String toString() {
		return "CombatLevelRequirement{" +
				"level=" + level +
				'}';
	}
}
