package org.logicail;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 23/02/14
 * Time: 17:14
 */
public class ScriptGui extends JFrame {
	public ScriptGui() throws HeadlessException {
		setTitle("Script GUI");
		setMinimumSize(new Dimension(400, 300));

		JTextArea output = new JTextArea();
		add(output);
	}

	public static void main(String[] args) throws FileNotFoundException {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final ScriptGui gui = new ScriptGui();
				gui.setVisible(true);
				gui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			}
		});
	}
}
