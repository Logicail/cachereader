package org.logicail;

import java.io.FileNotFoundException;

/**
 * Created with IntelliJ IDEA.
 * User: Logicail
 * Date: 23/02/14
 * Time: 16:33
 */
public class Slayer {
	public static void main(String[] args) throws FileNotFoundException {
//		npc("Grot1", 394, 1124184314, 13263);
//		npc("Grot2", 138, 1124184314, 14287);
//
//		npc("Cave slime1", 138, 1117892858, 14799);
//		npc("Cave slime2", 138, 1117892858, 12239);
//
//		npc("Bears1", 1, 20971520, 436);
//
//		npc("Minotaurs1", 138, 1090629882, 11215);
//
//		npc("Pyrefiends1", 552, 1107497098, 34767);
//		npc("Cockatrices1", 266, 1111601402, 22479);
//		npc("Cyclopses1", 266, 1128378618, 30159);
//		npc("Ghosts1", 138, 1092727934, 24527);
		//npc("Hobgoblins", 266, 1117892858, 33231);
		//npc("Crawling hand", 138, 1078046970, 23503);


		// Mike
		//npc("Minotaurs2", 138, 1092202746, 23199);
		//npc("Cave slime3", 138, 1119465722, 17055);
		//npc("Ghosts2", 138, 1094299898, 19615);

		int[] d = {1138340090,
				1077522682,
				1079619834,
				1085911290,
				1092202746,
				1092202746,
				1094299898,
				1094299898,
				1100591354,
				1113174266,
				1119465722,
				1125757178,
				1079619834,
				1094299898,
				1096397050,
				1102688506,
				1104785658,
				1113174266,
				1117368570,
				1121562874,
				1129951482,
				1132048634,
				1078046970,
				1090629882,
				1092727934,
				20971520,
				1094824186,
				1109594259,
				1117892858,
				1117892858,
				1124184314,
				1124184314,
				1088532730,
				1101115642,
				1107497098,
				1109504250,
				1111601402,
				1117892858,
				1128378618,
				1119990010};

		for (int i : d) {
			//System.out.println(getMaster(i));
			//System.out.println(getPoints(i));
			//System.out.println(getTaskId(i));
			System.out.println(get2091(i));
			//System.out.println(i & 0x1f);
			//System.out.println(i & 0x1f);
			//System.out.println((i >> 5) & 0x1f);
			//System.out.println(get2094(i));
			//System.out.println((i >> 0) & 0xff);
		}

//		int[][] masterToTask = {
//				{6, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,},
//				{30, 8, 21, 9, 22, 18, 13, 9, 26, 27, 14, 20, 10, 2}
//		};
//
//		for (int i = 0; i < masterToTask[0].length; i++) {
//			System.out.println(masterToTask[1][i] * masterToTask[0][i]);
//		}


//		int[] a = {22479, 24223
//
//
//		};
//
//		for (int i : a) {
//			//System.out.println(Integer.toBinaryString(i ^ 0b1111111));
//			System.out.println(i);
//			//twozeronineone(i);
//			//oneninezero(i);
//			twoonesevenzero(i);
//			//System.out.println(i ^ 0b1111111);
//		}
	}

	public static int getMaster(int value) {
		return (value >>> 17) & 0x3f;
	}

	public static int getTaskId(int value) {
		return (value >>> 21) & 0xff;
	}

	public static int getPoints(int value) {
		return value & 0x1ffff;
	}

	/*
	[id=2091]
	  [6636] ctx.settings.get(2091, 0, 0x3ff)
	  [6637] ctx.settings.get(2091, 10, 0x3ff)
	  [6638] ctx.settings.get(2091, 20, 0x3ff)
	  [9054] ctx.settings.get(2091, 0, 0xff)
	  [9055] ctx.settings.get(2091, 8, 0x1)
	  [9056] ctx.settings.get(2091, 9, 0x1)
	  [9057] ctx.settings.get(2091, 10, 0x1)
	  [9058] ctx.settings.get(2091, 11, 0x1)
	  [9059] ctx.settings.get(2091, 12, 0x1)
	  [9060] ctx.settings.get(2091, 13, 0x1)
	  [9061] ctx.settings.get(2091, 14, 0x1)
	  [9062] ctx.settings.get(2091, 15, 0x1)
	  [9063] ctx.settings.get(2091, 16, 0x1)
	  [9064] ctx.settings.get(2091, 17, 0x1)
	  [9065] ctx.settings.get(2091, 18, 0x1)
	  [9066] ctx.settings.get(2091, 19, 0x1)
	  [9067] ctx.settings.get(2091, 20, 0x1)
	  [9068] ctx.settings.get(2091, 21, 0xff)
	  [9069] ctx.settings.get(2091, 29, 0x1)
	  [9070] ctx.settings.get(2091, 30, 0x1)
	  [9084] ctx.settings.get(2091, 31, 0x1)
	 */
	public static String get2091(int value) {
		StringBuilder sb = new StringBuilder();

		sb.append(get(value, 0, 0x3ff)).append("\t");
		sb.append(get(value, 10, 0x3ff)).append("\t");
		sb.append(get(value, 20, 0x3ff)).append("\t");
		sb.append(get(value, 0, 0xff)).append("\t");
		sb.append(get(value, 8, 0x1)).append("\t");
		sb.append(get(value, 9, 0x1)).append("\t");
		sb.append(get(value, 10, 0x1)).append("\t");
		sb.append(get(value, 11, 0x1)).append("\t");
		sb.append(get(value, 12, 0x1)).append("\t");
		sb.append(get(value, 13, 0x1)).append("\t");
		sb.append(get(value, 14, 0x1)).append("\t");
		sb.append(get(value, 15, 0x1)).append("\t");
		sb.append(get(value, 16, 0x1)).append("\t");
		sb.append(get(value, 17, 0x1)).append("\t");
		sb.append(get(value, 18, 0x1)).append("\t");
		sb.append(get(value, 19, 0x1)).append("\t");
		sb.append(get(value, 20, 0x1)).append("\t");
		sb.append(get(value, 21, 0xff)).append("\t");
		sb.append(get(value, 29, 0x1)).append("\t");
		sb.append(get(value, 30, 0x1)).append("\t");
		sb.append(get(value, 31, 0x1));

		return sb.toString();
	}

	/*
	[id=2094]
	  [6645] ctx.settings.get(2094, 0, 0x3ff)
	  [6646] ctx.settings.get(2094, 10, 0x3ff)
	  [6647] ctx.settings.get(2094, 20, 0x3ff)
	  [9077] ctx.settings.get(2094, 0, 0xff)
	  [9078] ctx.settings.get(2094, 8, 0x7)
	  [9079] ctx.settings.get(2094, 11, 0x7)
	  [9080] ctx.settings.get(2094, 14, 0x1)
	  [9081] ctx.settings.get(2094, 15, 0xff)
	  [9082] ctx.settings.get(2094, 23, 0xff)
	  [9083] ctx.settings.get(2094, 31, 0x1)
	 */
	public static String get2094(int value) {
		StringBuilder sb = new StringBuilder();

		sb.append(get(value, 0, 0x3ff)).append("\t");
		sb.append(get(value, 10, 0x3ff)).append("\t");
		sb.append(get(value, 20, 0x3ff)).append("\t");
		sb.append(get(value, 0, 0xff)).append("\t");
		sb.append(get(value, 8, 0x7)).append("\t");
		sb.append(get(value, 11, 0x7)).append("\t");
		sb.append(get(value, 14, 0x1)).append("\t");
		sb.append(get(value, 15, 0xff)).append("\t");
		sb.append(get(value, 23, 0xff)).append("\t");
		sb.append(get(value, 31, 0x1));

		return sb.toString();
	}

	public static void npc(String header, int one_nine_zero, int two_zero_nine_one, int two_one_seven_zero) {
		System.out.println(header);

		System.out.println(190);
		oneninezero(one_nine_zero);

		System.out.println(2091);
		twozeronineone(two_zero_nine_one);

		System.out.println(2170);
		twoonesevenzero(two_one_seven_zero);

		System.out.println();
		System.out.println();
		System.out.println(two_zero_nine_one & 0x7F);

		System.out.println();
		System.out.println();
	}


	/*
[6639] ctx.settings.get(2092, 0, 0x3ff)
[6640] ctx.settings.get(2092, 10, 0x3ff)
[6641] ctx.settings.get(2092, 20, 0x3ff)

[9071] ctx.settings.get(2092, 0, 0x1ffff)
[9072] ctx.settings.get(2092, 17, 0x3f)
[9073] ctx.settings.get(2092, 23, 0xff)
	 */
	private static void twozeroninetwo(int value) {
		System.out.println(get(value, 0, 0x3ff));
		System.out.println(get(value, 10, 0x3ff));
		System.out.println(get(value, 20, 0x3ff));
		System.out.println();
		System.out.println(get(value, 0, 0x1ffff));
		System.out.println(get(value, 17, 0x3f));
		System.out.println(get(value, 23, 0xff));
	}

	private static void twozeronineone(int value) {
		System.out.println(get(value, 0, 0x3ff));
		System.out.println(get(value, 10, 0x3ff));
		System.out.println(get(value, 20, 0x3ff));
		System.out.println();
		System.out.println(get(value, 0, 0xff));
		System.out.println(get(value, 8, 0x1));
		System.out.println(get(value, 9, 0x1));
		System.out.println(get(value, 10, 0x1));
		System.out.println(get(value, 11, 0x1));
		System.out.println(get(value, 12, 0x1));
		System.out.println(get(value, 13, 0x1));
		System.out.println(get(value, 14, 0x1));
		System.out.println(get(value, 15, 0x1));
		System.out.println(get(value, 16, 0x1));
		System.out.println(get(value, 17, 0x1));
		System.out.println(get(value, 18, 0x1));
		System.out.println(get(value, 19, 0x1));
		System.out.println(get(value, 20, 0x1));
		System.out.println(get(value, 21, 0xff));
		System.out.println(get(value, 29, 0x1));
		System.out.println(get(value, 30, 0x1));
		System.out.println();
		System.out.println(get(value, 31, 0x1));
	}


	public static void twoonesevenzero(int value) {
		System.out.println(get(value, 9, 0x1ff));
		System.out.println();
		System.out.println(get(value, 0, 0x3));
		System.out.println(get(value, 2, 0x1));
		System.out.println(get(value, 6, 0x1));
		System.out.println(get(value, 3, 0x1));
		System.out.println(get(value, 4, 0x1));
		System.out.println(get(value, 5, 0x1));
		System.out.println(get(value, 7, 0x1));
		System.out.println(get(value, 8, 0x1));

		//What is ctx.settings.get(2170, 2, 0x7F)
		//What is ctx.settings.get(2170, 0, 0x3FF)
		System.out.println(get(value, 2, 0x7F));
		System.out.println(get(value, 0, 0x3FF));

	}

	/*
	[524] ctx.settings.get(190, 0, 0xf)
	[525] ctx.settings.get(190, 4, 0x7)
	[526] ctx.settings.get(190, 7, 0x7f)
	[527] ctx.settings.get(190, 14, 0xf)
	[528] ctx.settings.get(190, 18, 0x3)
	[529] ctx.settings.get(190, 20, 0x3)
	[530] ctx.settings.get(190, 22, 0x3)
	[531] ctx.settings.get(190, 24, 0x1f)

	[2837] ctx.settings.get(190, 0, 0x3f)
	[2838] ctx.settings.get(190, 6, 0x3f)
	[2839] ctx.settings.get(190, 12, 0x3f)
	[2840] ctx.settings.get(190, 18, 0x3f)
	[2841] ctx.settings.get(190, 24, 0x3f)

	[7938] ctx.settings.get(190, 0, 0x7f)
	[7939] ctx.settings.get(190, 7, 0x7f)
	[7940] ctx.settings.get(190, 14, 0x3ffff)
	 */
	public static void oneninezero(int value) {
		System.out.println(get(value, 0, 0xf));
		System.out.println(get(value, 4, 0x7));
		System.out.println(get(value, 7, 0x7f));
		System.out.println(get(value, 14, 0xf));
		System.out.println(get(value, 18, 0x3));
		System.out.println(get(value, 20, 0x3));
		System.out.println(get(value, 22, 0x3));
		System.out.println(get(value, 24, 0x1f));
		System.out.println();
		System.out.println(get(value, 0, 0x3f));
		System.out.println(get(value, 6, 0x3f));
		System.out.println(get(value, 12, 0x3f));
		System.out.println(get(value, 18, 0x3f));
		System.out.println(get(value, 24, 0x3f));
		System.out.println();
		System.out.println(get(value, 0, 0x7f));
		System.out.println(get(value, 7, 0x7f));
		System.out.println(get(value, 14, 0x3ffff));
	}

	public static int get(int value, int shift, int mask) {
		return (value >>> shift) & mask;
	}
}
